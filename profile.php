<div class="col-12">
    <div class="row">
        <div class="col-2 col-sm-3 mb-5 mt-5 user-list-profile avatar-card">
            <span class="online-status online"></span>
            <div class="avatar mb-2">
                <img src="/assets/images/avatar-1.png" class="border-radius">
            </div>
        </div>
        <div class="col-10 col-sm-9 mt-5">
            <h2 class="mt-4">John Smith</h2>
            <div class="profile-stars">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="far fa-star"></i>
            </div>
        </div>
    </div>
</div>


<div class="col-12 mb-5">
    <div class="profile-menu">
        <ul>
            <li><a href="">HAKKIMDA</a></li>
            <li class="active"><a href="">AYARLAR</a></li>
            <li><a href="">BİLDİRİMLER</a></li>
            <li><a href="">DİLEKÇELERİM</a></li>
            <li><a href="">TEVKİLLERİM</a></li>
            <li><a href="">PERSONELLER</a></li>
        </ul>
    </div>
</div>

<div class="col-12">

    <?php
    include 'parts/notification-row.php';
    include 'parts/notification-row-long.php';
    include 'parts/notification-row.php';
    ?>

</div>
<div class="col-md-12">
    <p class="text-center">
        <a href="" class="project-link">Çok daha fazlası için lütfen tıklayınız</a>
    </p>
</div>
