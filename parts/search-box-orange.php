<div class="col-8 offset-2 mt-5 mb-5">
    <div class="search-form search-form-orange">
        <form action="" class="form-inline">
            <input type="text" class="form-control" placeholder="Dilekçe Ara">
            <button type="submit" class="btn btn-warning text-white form-button"><i class="fa fa-search"></i></button>
        </form>
    </div>
</div>
