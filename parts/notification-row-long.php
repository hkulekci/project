<div class="row notification mb-5">
    <div class="col-2">
        <div class="avatar-card avatar-card-sm">
            <div class="avatar mb-2 ml-auto mr-auto">
                <span class="online-status online"></span>
                <img src="/assets/images/avatar-1.png" class="border-radius">
            </div>
            <a href="">John Simth</a>
        </div>
    </div>
    <div class="col-9">
        <div class="content rounded-box row">
            <div class="col-10">
                Merhabalar Adana da bir davam var ilgilenen arkadaşlar benimle iletişime geçerlerse çok sevinirim
                1500'lerden beri kullanılmakta olan standard Lorem Ipsum metinleri ilgilenenler için yeniden
                üretilmiştir. Çiçero tarafından yazılan 1.10.32 ve 1.10.33 bölümleri de 1914 H. Rackham çevirisinden
                alınan İngilizce sürümleri eşliğinde özgün biçiminden yeniden üretilmiştir. Merhabalar Adana da bir
                davam var ilgilenen arkadaşlar benimle iletişime geçerlerse çok sevinirim
                1500'lerden beri kullanılmakta olan standard Lorem Ipsum metinleri ilgilenenler için yeniden
                üretilmiştir. Çiçero tarafından yazılan 1.10.32 ve 1.10.33 bölümleri de 1914 H. Rackham çevirisinden
                alınan İngilizce sürümleri eşliğinde özgün biçiminden yeniden üretilmiştir
            </div>
            <div class="col-2">
                <a href=""><img src="/assets/icons/ic_question_answer_24px.png"></a> &nbsp;&nbsp;&nbsp;
                <a href=""><img src="/assets/icons/ic_done_all_24px.png"></a>
            </div>
        </div>
    </div>
    <div class="col-1 mt-4">
        <a href=""><img src="/assets/icons/ic_highlight_off_24px.png"></a>
    </div>
</div>
