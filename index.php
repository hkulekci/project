<?php
// Delegate static file requests back to the PHP built-in webserver
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}
?>
<?php
ob_start();

if (isset($_GET['page'])) {
    if (file_exists($_GET['page']. '.php')) {
        include_once $_GET['page']. '.php';
    }
} else {
    echo 'Content';
}

$content = ob_get_contents();
ob_end_clean();
include 'layout.php';
?>
