<html>
<head>
    <title>Application</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
    <div class="container">
        <header>

            <!-- <div class="user-menu float-right mt-2 hide">
                <a href=""><i class="fa fa-user fa-3x"></i></a>
                <ul class="user-submenu">
                    <li><a href=""><i class="fa fa-login"></i> Giriş Yap</a></li>
                    <li><a href=""><i class="fa fa-login"></i>  Kayıt Ol</a></li>
                </ul>
            </div> -->
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href=""><img src="/assets/images/logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active mr-4"><a href=""><img src="/assets/images/icons/homepage.png"> Anasayfa</a></li>
                        <li class="nav-item mr-4"><a href=""><img src="/assets/images/icons/about.png"> Hakkımızda</a></li>
                        <li class="nav-item mr-4"><a href=""><img src="/assets/images/icons/get-info.png"> Bilgi Al</a></li>
                        <li class="nav-item mr-4"><a href=""><img src="/assets/images/icons/blog.png"> Blog</a></li>
                        <li class="nav-item mr-4"><a href=""><img src="/assets/images/icons/contact.png"> İletisim</a></li>
                    </ul>
                </div>

            </nav>
        </header>

        <div class="left-navbar">
            <div class="ml-4 mt-3 mb-4 profile">
                <img src="/assets/images/avatar-1.png" align="left">
                <a href="">Haydar KULEKCI</a>
            </div>
            <div class="clearfix"></div>
            <ul class="navbar-nav ml-2 mb-3">
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/homepage-left.png"> Anasayfa</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/profile-left.png"> Profilim</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/password-change-left.png">  Şifre Değiştir</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/document-left.png">  Dilekçe Ara</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/tevkil-left.png">  Tevkil Ara</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/staff-search-left.png">  Personel Ara</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/friends-left.png">  Arkadaşlarım</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/inform-left.png">  Bildirim Yap</a></li>
                <li class="nav-item ml-4 mr-4 mb-2"><a href=""><img src="/assets/images/icons/message-send-left.png">  Mesaj Gönder</a></li>
            </ul>
        </div>

        <div class="row">
            <?php echo $content; ?>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="application/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</body>
</html>
