<div class="col-md-12  text-center">
    <div class="row">
        <?php for($i = 0; $i < 20; $i++): ?>
        <div class="col mb-5 user-list-profile avatar-card mr-auto ml-auto">
            <div class="avatar mb-2">
                <span class="online-status <?php echo ($i%2) ? 'online' : 'offline' ?>"></span>
                <img src="/assets/images/avatar-1.png" class="border-radius">
            </div>
            <a href="">John Simth</a>
        </div>
        <?php endfor; ?>
    </div>
</div>
<div class="col-md-12">
        <p class="text-center">
            <a href="" class="project-link">Çok daha fazlası için lütfen tıklayınız</a>
        </p>
</div>
