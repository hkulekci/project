<?php
include 'parts/search-box-orange.php';
?>
<div class="col-12">
    <div class="row doc-list">
        <?php for($i = 0; $i < 20; $i++): ?>
        <div class="mr-2 mb-3 doc-item">
            <div class="profile-image">
                <img src="/assets/images/avatar-1.png">
            </div>
            <div class="preview"><img src="/assets/images/doc.png" alt=""></div>
            <div class="actions">
                <a href="" data-toggle="tooltip" data-placement="top" title="1600 Kez görüntülendi"><img src="/assets/images/icons/visibility-doc.png"></a>
                <a href=""><img src="/assets/images/icons/spellcheck-doc.png"></a>
                <a href="" data-toggle="tooltip" data-placement="top" title="<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='far fa-star'></i><i class='far fa-star'></i>" data-html="true"><img src="/assets/images/icons/star-doc.png"></a>
                <a href=""><img src="/assets/images/icons/delete-doc.png"></a>
            </div>
        </div>
        <?php endfor; ?>
    </div>
</div>
